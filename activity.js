// #3 using insertOne method ==============>

db.rooms.insertOne({
    "name": "single",
    "price": 1000,
    "description": "A simple room with all the basic necessities",
    "rooms_available": 10,
    "isAvailable": false
});

// #4 using insertMany method ==============>

db.rooms.insertMany([{
    "name": "double",
    "accomodates": "3",
    "price": 2000,
    "description": "A room fit for a small family going on a vacation",
    "rooms_available": 15,
    "isAvailable": false
},
{
    "name": "queen",
    "accomodates": "4",
    "price": 4000,
    "description": "A room with a queen sized bed perfect for a simple getaway",
    "rooms_available": 15,
    "isAvailable": false
}]);

// #5 using find method ==============>
db.rooms.find({"name": "double"});

// #6 using updateOne ===============>
db.rooms.updateOne(
        { "name": "queen" },
        {
            $set: {
            "rooms_available": 0,
            }
        }
);

// #7 using deleteMany =============>

db.rooms.deleteMany(
    { "rooms_available": 0 }
);

